#!/usr/bin/env python3

# Config:

# Change to your mouse, find it via `cat /proc/bus/input/devices`
mouseDev = '/dev/input/event9'
# How much mouse movement should affect joystick
sensitivity = 30
# Range from 0 of joystick
joystickRange = 512
# Seconds after last mouse movement to settle joystick back to center
joystickTimeout = 0.1
# Seconds after mouse wheel movement to hold button before letting go
wheelTimeout = 0.27
# Name for virtual joystick/gamepad
joystickName = 'mouse2joy'
buttons = {
	# Mouse buttons
	'BTN_LEFT':   'BTN_A',
	'BTN_RIGHT':  'BTN_B',
	'BTN_MIDDLE': 'BTN_C',
	# Mouse wheel
	'WHEEL_UP':   'BTN_X',
	'WHEEL_DOWN': 'BTN_Y',
	# My mouse can click the scroll wheel left or right
	'BTN_SIDE':   'BTN_Z',
	'BTN_EXTRA':  'BTN_SELECT',
	}
# Button on mouse to toggle joystick mode
toggleButton = 'KEY_LEFTMETA'
#toggleButton = 'BTN_MIDDLE' # If you don't have any other buttons on your mouse

# End of config

from evdev import InputDevice
mouse = InputDevice(mouseDev)
from evdev.ecodes import ecodes
buttonCodes = {}
for button in buttons:
	try: buttonCodes[ecodes[button]] = button
	except KeyError: pass

import uinput as ui
events = (ui.BTN_GAMEPAD, ui.BTN_A, ui.BTN_B, ui.BTN_C, ui.BTN_X, ui.BTN_Y, ui.BTN_Z, ui.BTN_SELECT, ui.ABS_X + (0, joystickRange-1, 0, 0), ui.ABS_Y + (0, joystickRange-1, 0, 0), )
joystick = ui.Device(events, name=joystickName)
joystickCenter = int(joystickRange/2-1)
joystickAxis = {'X': ui.ABS_X, 'Y': ui.ABS_Y}
def settle(a): joystick.emit(joystickAxis[a], joystickCenter)
settle('X')
settle('Y')

from threading import Timer as TTimer
class Timer(TTimer):
	def __init__(self, seconds, function, args):
		self.t = TTimer(seconds, function, args)
		self.seconds = seconds
		self.function = function
		self.args = args
	
	def delay(self):
		self.cancel()
		self.t = TTimer(self.seconds, self.function, self.args)
		self.start()
	def end(self):
		self.cancel()
		self.function(*self.args)
		self.t = TTimer(self.seconds, self.function, self.args)
	def cancel(self): self.t.cancel()
	def start(self):  self.t.start()

timers = {
	'X': Timer(0.1, settle, 'X'),
	'Y': Timer(0.1, settle, 'Y'),
	'WHEEL_UP':   Timer(wheelTimeout, joystick.emit, (ui.BTN_X, 0)),
	'WHEEL_DOWN': Timer(wheelTimeout, joystick.emit, (ui.BTN_Y, 0))
	}

toggle = False

for event in mouse.read_loop():
	if event.type == ecodes['EV_KEY']:
		if event.code in buttonCodes:
			if toggle: joystick.emit((1, ecodes[buttons[buttonCodes[event.code]]]), event.value)
		elif event.code == ecodes[toggleButton]:
			if event.value == 0:
				toggle = not toggle
				if toggle: mouse.grab()
				else:
					mouse.ungrab()
					settle('X')
					settle('Y')
		else: print('Non-configured button pressed: '+str(event.code))
	elif event.type == ecodes['EV_REL'] and toggle:
		if event.code == ecodes['REL_WHEEL']:
			# wheel up
			if event.value == 1:
				timers['WHEEL_DOWN'].end()
				joystick.emit((1, ecodes[buttons['WHEEL_UP']]), 1)
				timers['WHEEL_UP'].delay()
			# wheel down
			elif event.value == -1:
				timers['WHEEL_UP'].end()
				joystick.emit((1, ecodes[buttons['WHEEL_DOWN']]), 1)
				timers['WHEEL_DOWN'].delay()
		else:
			if   event.code == ecodes['REL_X']: axis = 'X'
			elif event.code == ecodes['REL_Y']: axis = 'Y'
			else: continue
			joystick.emit(joystickAxis[axis], int((event.value*sensitivity)+joystickCenter))
			timers[axis].delay()
